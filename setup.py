from setuptools import setup

setup(
    name='VPTrac',
    version='0.01',
    packages=['vptrac'],
	package_data={'vptrac' : ['templates/*.html', 'htdocs/js/*.js', 'htdocs/css/*.css']},
    author='Leslie Harlley Watter',
    license='BSD',
    url='http://gitorious.org/vptrac/vptrac',
    description='Vocabulário Padrão for Trac',
    entry_points = {'trac.plugins': ['vptrac = vptrac']},
    dependency_links=['http://svn.edgewall.org/repos/genshi/trunk#egg=Genshi-dev'],
    install_requires=['Genshi >= 0.5.dev-r698,==dev'],
    )
